## Header
**Header**, ou cabeçalho, é um tipo de marcação de texto para deixar o código mais organizado.
No gitlab, eles podem ter 3 variações de tamanho:

 - Heading 2, sendo a maior permitida pela plataforma;

 - Heading 3, sendo a intermediária;

 - Heading 4, sendo a menor entre as três.

*Nota: Cada header deve conter o numero de "HASHES" (ou "#") equivalente ao número do seu header antes do texto.

## Quebras h2
Nas quebras de linhas, pode-se usar o mesmo comando utilizado em HTML, Break Row ou "< br >"

## Negrito / Italico h2

**Bold**, ou negrito, é a forma de destacar uma palavra ou frase e dar mais ênfase. (" ** ** ")
<br>
_italic_, ou italico, é a forma de deixar a palavra ou frase de forma cursiva.("_ _")
<br>
***Bold + italic***  mistura ambos. ("texto entre 3x*")
<br>


## Links h2

Link inline: "[ texto entre chaves ] (link) {: target"_blank" } 

<br>


## Listas h2

Lista não ordenada: 

A lista não ordenada pode ser utilizada através do sinal de subtração antes do item a ser listado:
<br>

- item 1
    - subitem 1
    - subitem 2
    - subitem 3
<br>

- item 2 ...

<br>

Lista ordenada: 

Lista ordenada pode ser utilizada pelo número acompanhado por um ponto sem espaço (Ex: 1. )

<br>

1. Item 1
    1. subitem 1
    2. subitem 2
    3. subitem 3

<br>


